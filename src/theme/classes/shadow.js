import { createGlobalStyle } from "styled-components";

import colors from "@theme/colors";

export default createGlobalStyle`
  .shadow-container {
    background: ${props => colors[props.theme.name].secundaryMenuBackground};
    border: 1px solid ${props => colors[props.theme.name].background};
    box-shadow: 1px 1px 1px 1px ${colors.black}82;
    padding: 1rem;
  }

  .blur {
    filter: blur(0.25em);
  }
`