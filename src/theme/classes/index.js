import backgroundColors from './backgroundColors'
import shadow from './shadow'
import fontColors from './fontColors'

export default [
  backgroundColors,
  shadow,
  fontColors,
]