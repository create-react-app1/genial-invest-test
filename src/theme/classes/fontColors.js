import { createGlobalStyle } from "styled-components";

import colors from "@theme/colors";

export default createGlobalStyle`
  .font-color-negative {
    color: ${colors.persianRed} !important;
  }

  .font-color-positive {
    color: ${colors.limeGreen} !important;
  }
`