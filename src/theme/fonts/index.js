import { createGlobalStyle } from "styled-components";
import fontsConfig from "./fontsConfig";

const names = (() => {
  let auxNames = {};

  fontsConfig.forEach(font => {
    auxNames[font.name.toLowerCase()] = font.name;
  });

  return auxNames;
})();

const faces = 
  createGlobalStyle`
  ${fontsConfig
  .map(
    font => `@font-face { font-family: ${font.name}; src: url(${font.url});}`
  )
  .join(" ")}`

export default {
  names,
  Faces: faces,
};
