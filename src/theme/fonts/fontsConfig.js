import font_SourceSansPro from '@fonts/Source_Sans_Pro/SourceSansPro-Regular.ttf'

/*
  To add any font into the project:

  {
    name: "Name of Font",
    url: "font url imported",
  }
  
*/

export default [
  {
    name: "Source Sans Pro",
    url: font_SourceSansPro,
  },
];
