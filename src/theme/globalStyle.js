import { createGlobalStyle } from "styled-components";
import myTheme from '@theme'

export default createGlobalStyle`
  body {    
    margin: 0;

    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;

    background-color: ${props => myTheme.colors[props.theme.name].background} !important;
  }

  :root {
    font-size: 60%;
  }
  
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }
  
  body, 
  input, 
  button,
  textarea,
  label,
  p {
    font: 500 1.6rem Source Sans Pro;
    color: ${props => myTheme.colors[props.theme.name].primary};
  }

  button,
  input,
  textarea {
    outline: none;
    background: none;
    border: none;
  }

  a {
    text-decoration: none;
    cursor: pointer;
  }

  button {
    cursor: pointer;
  }
  
  code {
    font-family: source-code-pro, Menlo, Monaco, Consolas, "Courier New",
      monospace;
  }

  @media (min-width: 1200px) {
    .container {
        max-width: 1200px !important;
    }
  }

  img, picture, video, embed {
    max-width: 100%;
  }
`
