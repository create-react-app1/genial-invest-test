/**
  Colors

  Obs: Mostly of the color variables names are from aproximates names.
  Reference: https://www.htmlcsscolor.com
**/

let colors = { 
  // General Colors
  midnightExpress: '#0c0b38',
  sapphire: '#142364',
  snow: '#fcfcfc',
  santasGrey: '#9494a8',
  white: '#FFFFFF',
  silver: '#bababa',
  black: '#000000',

  // Especific Colors
  freeSpeechBlue: '#364dcd', // icon logo

  limeGreen: '#2abf28', // GREEN positive numbers
  persianRed: '#db2828', // RED negative numbers

  dodgerBlue: '#0099ff', // FUNDOS
  royalBlue: '#3d58f2', // RENDA FIXA
  turquoise: '#37dcf6', // AÇÕES / FUTUROS
  echoBlue: '#9cb1c9', // TESOURO
  midnightExpress_light: '#0a1240', // PREVIDENCIA
}

export default {
  ...colors,

  main: {
    primary: colors.black,
    primaryLight: colors.silver,
    secundary: colors.midnightExpress,

    background: colors.snow,

    menuBackground: colors.midnightExpress,
    menuBackgroundSelected: colors.sapphire,

    menuLink: colors.santasGrey,
    menuLinkSelected: colors.white,

    secundaryMenuBackground: colors.white,
  },

  dark: {
    primary: colors.white,
    primaryLight: colors.silver,
    secundary: colors.santasGrey,

    background: colors.sapphire,

    menuBackground: colors.midnightExpress,
    menuBackgroundSelected: colors.sapphire,

    menuLink: colors.santasGrey,
    menuLinkSelected: colors.white,

    secundaryMenuBackground: colors.midnightExpress,
  }
};
