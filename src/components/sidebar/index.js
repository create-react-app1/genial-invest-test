import React from 'react'
import { Link } from 'react-router-dom'

// Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { changeTheme } from '@actions';

import Style from './style.js'

// Images
import genialLogo from '../../assets/images/logo_genial.png'

// Config
import config from './config'

const Sidebar = (props) => {

  const changeTheme = () => {
    props.changeTheme(props.theme === 'main' ? 'dark' : 'main')
  }

  return (
    <Style className="sidebar grid-container">
      <div className="logo-container" onClick={changeTheme}>
        <img src={genialLogo} className="genial-invest-logo" alt="Logo Genial Invest"/>
      </div>
      <ul className="navbar-menu">
      {
        config.map((item, id) => {
          const Icon = item.icon
          return (
            <Link to={item.url} key={id} >
              <li className={`item-menu ${id === 1 ? 'selected' : ''}`}>
                  <Icon className="icon-menu" />
                  <label className="label-menu">{item.label}</label>
              </li>
            </Link>
          )
        })
      }
      </ul>
    </Style>
  )
}

const mapStateToProps = store => ({
  theme: store.themeState.theme,
})

const mapDispatchToProps = dispatch => 
  bindActionCreators({
    changeTheme,
  }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar)