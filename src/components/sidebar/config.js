import { 
  HomeOutlined,
  AccountBalanceWalletOutlined,
  MonetizationOnOutlined,
  AccountBalanceOutlined,
  ReceiptOutlined,
  ImportContactsOutlined,
  SettingsRemoteOutlined,
  TuneOutlined,
  LiveTvOutlined,

} from '@material-ui/icons';

export default [
  {label: "Inicio", icon: HomeOutlined, url: "#"},
  {label: "Carteira", icon: AccountBalanceWalletOutlined, url: "#"},
  {label: "Investimentos", icon: MonetizationOnOutlined, url: "#"},
  {label: "Resgaste", icon: AccountBalanceOutlined, url: "#"},
  {label: "Estrato", icon: ReceiptOutlined, url: "#"},
  {label: "Termos", icon: ImportContactsOutlined, url: "#"},
  {label: "Trader Cockpit", icon: SettingsRemoteOutlined, url: "#"},
  {label: "Homebroker", icon: TuneOutlined, url: "#"},
  {label: "Sala ao Vivo", icon: LiveTvOutlined, url: "#"},
]