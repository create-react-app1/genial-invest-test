import styled from 'styled-components'
import myTheme from '../../theme/index.js'

export default styled.section`

  background-color: ${props => myTheme.colors[props.theme.name].menuBackground};

  .logo-container {
    height: 8rem;
    box-shadow: -2px 1px 2px 0px #0000008a;
    display: flex;
    cursor: pointer;
  }

  img.genial-invest-logo {
    margin: auto;
    width: 40%;
  }

  .item-menu {
    height: 5rem;
    padding: 5px;
    display: flex;
    align-items: center;
  }

  .icon-menu {
    color: ${props => myTheme.colors.freeSpeechBlue};
    font-size: 3rem;
    margin-right: 1rem;
  }

  .label-menu {
    color: ${props => myTheme.colors[props.theme.name].menuLink};
    font-size: 1.6rem;
  }

  .item-menu.selected, .item-menu:hover {
    background-color: ${props => myTheme.colors[props.theme.name].menuBackgroundSelected};

    .label-menu {
      color: ${props => myTheme.colors[props.theme.name].menuLinkSelected};
    }
  }


`
