import styled from 'styled-components'
import myTheme from '@theme'

export default styled.section`
  display: flex;
  align-items: center;
  background-color: ${myTheme.colors.freeSpeechBlue};
  box-shadow: 0px 1px 1px 1px ${myTheme.colors.freeSpeechBlue};

  &&.tabs.grid-container {
    display: grid;
    grid-template-columns: repeat(1, 2fr) 1fr;
    gap: 1.6rem;
  }

  .summary-item {
    margin: 1rem;
    color: ${props => myTheme.colors[props.theme.name].menuLinkSelected};
  }

  .summary-item.selected, .summary-item:hover {
    font-weight: bold;
  }

  .summary-item.selected::after, .summary-item:hover::after {
    content: ' ';
    width: 50%;
    height: 2px;
    background-color: ${props => myTheme.colors[props.theme.name].menuLinkSelected};
    position: absolute;
    left: 0;
    bottom: -.2rem;
  }

  .grid-item:last-child {
    text-align: right;
  }

`