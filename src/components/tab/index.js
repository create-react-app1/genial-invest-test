import React from 'react'

import Style from './style'

export default () => {
  const tabs = [
    { label: 'Resumo', },
    { label: 'Moivementações', },
    { label: 'Posição', },
    { label: 'Saque', },
    { label: 'Monte Sua Carteira', },
  ]

  return (
    <Style className="tabs grid-container">
      <div className="grid-item">
      {
        tabs.slice(0, 4).map((t, id) => {
          return (
            <button key={id} className={`summary-item ${id === 0 ? 'selected' : ''}`}>{t.label}</button>
          )
        })
      }
      </div>
      <div className="grid-item">
        <button key={4} className={`summary-item`}>{tabs[4].label}</button>
      </div>
    </Style>
  )
}