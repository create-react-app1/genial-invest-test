import React from 'react'

// Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setHideValues } from '@actions';

import { Grid, Checkbox } from '@material-ui/core'
import FormControlLabel from '@material-ui/core/FormControlLabel';


import Style from './style'

import pizzaChartImg from '../../assets/images/pizza-chart.png'
import lineChartImg from '../../assets/images/line-chart.png'

const summaryContent = (props) => {

  const { hide } = props 
  const hideValue = '000.000.000,00'
  const hideClass = 'blur'

  const profitabilityCategoryItems = [
    {title: 'Fundos', myClass: 'fundos'},
    {title: 'Renda Fixa', myClass: 'renda_fixa'},
    {title: 'Ações / Futuro', myClass: 'acoes_futuro'},
    {title: 'Tesouro', myClass: 'tesouro'},
    {title: 'Previdência', myClass: 'previdencia'},
  ]

  const periods = ['Mês', 'Ano', '12 meses']

  const profitabilityTotalCard = () => {
    return (
      <Grid container className="profits-percent">
        <Grid item key='title' md={5}>
          <strong className="title">Rentabilidade na Carteira</strong>
        </Grid>
      {
        periods.map((p, i) =>
        <Grid item key={i} md={2}>
          <label>{p}</label>
          <label className={`percent-value font-color-${Math.random() > .5 ? 'positive':'negative'}`}>+ 5%</label>
        </Grid>
        )
      }
      </Grid>
    )
  }

  const activeProfits = () => {
    return (
      <Grid container className="active-profits" justify="center">
        {
          profitabilityCategoryItems.map((item, id) => {
            return (
              <Grid item key={id} md={4} className={`summary-profit ${item.myClass}`}>
                <label>{item.title}</label>
                <p>10% | 2 ativos</p>
              </Grid>
            )
          })
        }
      </Grid>
    )
  }

  const profitabilityCategoryCard = (item) => {
    return (
      <article key={item.myClass} className={`shadow-container item ${item.myClass}`}>
        <strong>{item.title}</strong>
        <label className="value">
          R$<span className={`${hide ? hideClass : ''}`}>{hide ? hideValue : '5.000.000,00'}</span>
        </label>
        <hr />
        <Grid container className="profits-percent">
        {
          periods.map((p, i) =>
          <Grid item key={i} md={4}>
            <label>{p}</label>
            <label className={`percent-value font-color-${Math.random() > .5 ? 'positive':'negative'}`}>+ 5%</label>
          </Grid>
          )
        }
        </Grid>
      </article>
    )
  }

  return(
    <Style className="content grid-container">
      <section className="pizza-chart">
        <article className="shadow-container">
          <strong className="title">Minha Carteira</strong>
          <div className="pizza-chart-wrapper">
            <section className="pizza-chart-infos">
              <img src={pizzaChartImg} />
              <div className="value-infos">
                <label className="value">
                  R$<span className={`${hide ? hideClass : ''}`}>{hide ? hideValue : '500.000,00'}</span>
                </label>
                <label>22 ativos</label>
                <hr />
                <label>Valor total bruto</label>
              </div>
            </section>
            { activeProfits() }
          </div>
        </article>
      </section>
      
      <section className="profitability-total">
        <article className="shadow-container">
          { profitabilityTotalCard() }
        </article>
      </section> 

      <section className="line-chart">
        <article className="shadow-container">
          <strong className="title">Gráfico de Rentabilidade</strong>
          <img src={lineChartImg} />
          <Grid container className="active-profits" justify="space-between" alignItems="center">
            <Grid item key='title' md={2} className="">
              <label>Comparativos:</label>
            </Grid>
            <Grid item key='options' md={6} className="">
              <FormControlLabel control={<Checkbox name="cdi" color="primary" />} label="CDI" />
              <FormControlLabel control={<Checkbox name="poupanca" color="primary" />} label="Poupança" />
              <FormControlLabel control={<Checkbox name="ibov" color="primary" />} label="IBOV" />
              <FormControlLabel control={<Checkbox name="ipca" color="primary" />} label="IPCA" />
            </Grid>
            <Grid item key='description' md={4} className="">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc orci velit, posuere ut augue nec, ultricies auctor ex. Donec aliquet dui eu lectus congue, gravida pulvinar nibh consectetur. Fusce tellus lectus, elementum eu ipsum eget, finibus eleifend lorem. Suspendisse maximus tincidunt velit id vehicula</p>
            </Grid>
          </Grid>
        </article>
      </section>

      <section className="profitability-category">
      { profitabilityCategoryItems.map(item => profitabilityCategoryCard(item)) }
      </section>
    </Style>
  )
}

const mapStateToProps = store => ({
  hide: store.hideValuesState.hide,
})

const mapDispatchToProps = dispatch => 
  bindActionCreators({
    setHideValues,
  }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(summaryContent)