import styled from 'styled-components'
import myTheme from '@theme'

export default styled.section`
  &&.content.grid-container {
    display: grid;
    grid-template-columns: 2fr 3fr;
    grid-template-rows: auto auto auto;
    gap: 1.6rem;
    padding: 1.6rem;
    grid-template-areas: 
      "pizza-chart profitability-total" 
      "pizza-chart line-chart" 
      "profitability-category profitability-category";
  }

  &&.content.grid-container * { 
    // border: 1px solid green;
    position: relative;
  }

  &&.content.grid-container section > * {
    height: 100%;
  }

  .pizza-chart { grid-area: pizza-chart;}

  .profitability-total { 
    grid-area: profitability-total; 
  
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    gap: 1.6rem;
  }

  .line-chart { grid-area: line-chart; }

  .profitability-category { 
    grid-area: profitability-category; 

    display: grid;
    grid-template-columns: repeat(5, 1fr);
    gap: 1.6rem;
  }

  .title {
    font-size: 1.4rem;
    font-weigth: bold;
    color: ${props => myTheme.colors[props.theme.name].menuLink};
  }

  .profits-percent {
    align-items: center;
    height: 100%;
  }

  .profits-percent label {
    display: block;
    font-size: 1.4rem;
  }

  .profits-percent .percent-value {
    font-weight: bold;
  }

  .pizza-chart .pizza-chart-wrapper {
    padding-bottom: 1.6rem;
    top: 50%;
    transform: translateY(-50%);
  }

  .pizza-chart-infos {
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .pizza-chart-infos .value-infos {
    position: absolute !important;
    
    transform: translateY(-50%);
    top: 50%;
    height: auto !important;

    text-align: center;
  }

  .pizza-chart-infos .value-infos > * {
    display: block;

    margin: 0.8rem 0;
    font-size: 1.4rem;

    color: ${props => myTheme.colors[props.theme.name].menuLink};
  }

  .pizza-chart-infos .value-infos .value {
    color: ${props => myTheme.colors[props.theme.name].primary};
    font-weight: bold;
  }

  .summary-profit {
    text-align: center;
    padding: .5rem;
  }

  .summary-profit::before {
    content: ' ';
    width: 3.6rem;
    height: .5rem;
    border-radius: 10px;
    margin: .5rem auto;
    display: flex;
  }

  .summary-profit.fundos::before { background-color: ${myTheme.colors.dodgerBlue}; }
  .summary-profit.renda_fixa::before { background-color: ${myTheme.colors.royalBlue}; }
  .summary-profit.acoes_futuro::before { background-color: ${myTheme.colors.turquoise}; }
  .summary-profit.tesouro::before { background-color: ${myTheme.colors.echoBlue}; }
  .summary-profit.previdencia::before { background-color: ${myTheme.colors.midnightExpress_light}; }

  .summary-profit label {
    font-weight: bold;
  }

  .summary-profit p {
    color: ${props => myTheme.colors[props.theme.name].menuLink};
  }

  .line-chart img {
    margin: 1.6rem 0;
  }

  .line-chart .active-profits {
    padding: 1.6rem;

    label, span.MuiFormControlLabel-label, p {
      font: 500 1.4rem Source Sans Pro !important;
      color: ${props => myTheme.colors[props.theme.name].menuLink};
    }

    label { font-weight: bold !important; }
    p { font-size: 1rem !important; }
  }

  .profitability-category {
    overflow-x: auto;
  }

  .profitability-category strong {
    border-radius: 3rem;
    padding: .2rem .8rem;
    font-size: 1.2rem;
    text-transform: uppercase;
    font-weight: bold;
    color: ${props => myTheme.colors[props.theme.name].menuLinkSelected};
  }
  .profitability-category .fundos strong { background-color: ${myTheme.colors.dodgerBlue}; }
  .profitability-category .renda_fixa strong { background-color: ${myTheme.colors.royalBlue}; }
  .profitability-category .acoes_futuro strong { background-color: ${myTheme.colors.turquoise}; }
  .profitability-category .tesouro strong { background-color: ${myTheme.colors.echoBlue}; }
  .profitability-category .previdencia strong { background-color: ${myTheme.colors.midnightExpress_light}; }

  .profitability-category .value {
    display: block;
    margin: 1.6rem 0;
    font-size: 1.8rem;
  }

  .profitability-category .profits-percent {
    height: auto;
    margin-top: 1.6rem;
  }

  .profitability-category .profits-percent label {
    font-size: 1.2rem;
    color: ${props => myTheme.colors[props.theme.name].menuLink};
  }

  .profitability-category .profits-percent .percent-value {
    font-weight: bold;
  }

`