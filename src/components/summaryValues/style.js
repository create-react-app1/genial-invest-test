import styled from 'styled-components'

export default styled.section`

  display: flex;
  align-items: center;

  padding: 1rem;

  .summary-item label {
    display: block;
  }

  .subject {
    font-weight: bold;
    font-size: 1.4rem;
  }

  .value {
    font-size: 2.3rem;
  }

`