import React from 'react'

// Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setHideValues } from '@actions';

import { Grid } from '@material-ui/core'

import Style from './style'

const summaryValues = (props) => {

  const { hide } = props 
  const hideValue = '000.000.000,00'
  const hideClass = 'blur'

  const boxes = [
    { label: 'Saldo em conta corrente', value: '5.643.764,00'},
    { label: 'Disponível para investir', value: '5.643.764,00'},
    { label: 'Patrimônio total', value: '5.643.764,00'},
  ]

  return (
    <Style className="summary grid-container"> 
      {
        boxes.map((box, id) => {
          return (
            <Grid item key={id} sm={6} md={4} lg={2} className="summary-item">
              <label className="subject">{box.label}</label>
              <label className="value">
                  R$<span className={`${hide ? hideClass : ''}`}>{hide ? hideValue : box.value}</span>
                </label>
            </Grid>
          )
        })
      }
    </Style>
  )
}

const mapStateToProps = store => ({
  hide: store.hideValuesState.hide,
})

const mapDispatchToProps = dispatch => 
  bindActionCreators({
    setHideValues,
  }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(summaryValues)