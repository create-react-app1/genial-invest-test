import styled from 'styled-components'

import myTheme from '@theme'

export default styled.section`

  background-color: ${props => myTheme.colors[props.theme.name].secundaryMenuBackground};
  
  display: flex;
  justify-content: space-between;
  align-items: center;
  box-shadow: 0px 1px 1px 1px #0c0c0c7d;

  height: 8rem;  

  .search {
    display: flex;
    height: 3.6rem;
    margin-left: 1rem;
    margin-right: 5rem;
    color: ${props => myTheme.colors[props.theme.name].menuLink}; 
  }

  .search_txt {
    height: 100%;
    border-radius: 30px 0 0 30px;
    background-color: ${props => myTheme.colors[props.theme.name].background};
    padding: .5rem 2rem;
    color: ${props => myTheme.colors[props.theme.name].menuLink};
    border: none;
    font-size: 1.4rem
  }

  .search .search-icon {
    height: 100%;
    border-radius: 0 30px 30px 0;
    width: 3.6rem;
    padding: 5px;
    background-color: ${props => myTheme.colors[props.theme.name].background};
  }

  .user-menu {
    display: grid;
    gap: 1.6rem;
    grid-template-columns: repeat(4, auto);
    align-items: center;
  }

  .hide-values_btn {
    display: flex;
    align-items: center;
    border-radius: 30px;
    padding: 0.2rem 1.6rem;
    color: ${props => myTheme.colors[props.theme.name].menuLink};
    height: 3.6rem;
  }

  .hide-values_btn label {
    color: ${props => myTheme.colors[props.theme.name].menuLink};
    font-size: 1.4rem
  }

  .hide-values_btn .icon {
    width: 2rem;
    height: 2rem;
    border-radius: 50%;
    background-color: ${props => myTheme.colors[props.theme.name].menuLink};
    color: ${props => myTheme.colors[props.theme.name].menuLinkSelected};
    padding: 0.2rem;
    margin-left: 1.6rem;
  }

  .bell_btn {
    height: 3.6rem;
    width: 3.6rem;
    border: none;
    background: none;
  }

  .bell_btn .icon {
    height: 100%;
    width: 100%;
    color: ${props => myTheme.colors[props.theme.name].menuLink};
  }

  .bell_btn .notification_ball {
    height: 1rem;
    width: 1rem;
    top: 2.5rem;
    left: 2.5rem;
    border-radius: 50%;
    position: absolute;
    background-color: ${myTheme.colors.persianRed};
  }

  .grid-container-user {
    display: grid;
    align-items: center;
    grid-template-columns: 1fr 3fr;
    grid-template-rows: repeat(2, 1fr);
    gap: .5rem;
    grid-template-areas: 
      "innitals first-name" 
      "innitals code-number";

    * {
      font-size: 1.4rem
    }
  }
  
  .innitals { 
    grid-area: innitals;
    
    text-align: center;
    border-radius: 50%;
    padding: 0.6rem;
    background-color: ${props => myTheme.colors[props.theme.name].secundary};
    color: ${myTheme.colors.white};
    font-weight: bold;
    border: 1px solid ${myTheme.colors.silver};
  }
  
  .first-name { 
    grid-area: first-name;
    font-weight: bold;
  }
  
  .code-number { grid-area: code-number; }

  .logoff_btn {
    right: 0px;
    width: 5rem;
    height: 3.6rem;
    border-radius: 30px 0 0 30px;
    background-color: ${props => myTheme.colors[props.theme.name].secundary};
    text-align: left;

    align-items: center;
    display: flex;
  }

  .logoff_btn .icon {
    margin-left: 1rem;
    font-size: 2.5rem;
    color: ${props => myTheme.colors[props.theme.name].secundaryMenuBackground};
  }

`