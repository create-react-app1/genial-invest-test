import React, { useState } from 'react'

// Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setHideValues } from '@actions';

import Style from './style'

import {
  SearchOutlined,
  VisibilityOutlined,
  VisibilityOffOutlined,
  NotificationsNoneOutlined,
  PowerSettingsNewOutlined, 
} from '@material-ui/icons';

const Header = (props) => {

  const [hide, setHide] = useState(props.hide);
  const btns = {
    toShow: {
      label: "MOSTRAR VALORES",
      icon: VisibilityOutlined
    },
    toHide: {
      label: "ESCONDER VALORES",
      icon: VisibilityOffOutlined
    }
  }

  const currentShowHideBtn = hide ? btns.toShow : btns.toHide

  const toogleHide = () => {
    props.setHideValues(!hide)
    setHide(!hide)
  }

  const getIconComponent = (Icon) => <Icon className="icon"/>

  return (
    <Style className="header grid-container">
      <section className="search">
        <input className="search_txt" type="text" placeholder="O que você procura?" />
        <SearchOutlined className="search-icon"/>
      </section>

      <section className="user-menu">
        <button className="shadow-container hide-values_btn" onClick={toogleHide}>
          <label>{currentShowHideBtn.label}</label>
          { getIconComponent(currentShowHideBtn.icon) }
        </button>

        <button className="bell_btn">
          <NotificationsNoneOutlined className="icon" />
          <div className="notification_ball" />
        </button>

        <section className="grid-container-user user">
          <label className="innitals">GR</label>
          <label className="first-name">Giovanni</label>
          <label className="code-number">CCC 02091992</label>
        </section>

        <button className="logoff_btn">
          <PowerSettingsNewOutlined className="icon" />
        </button>
      </section>
    </Style>
  )
}

const mapStateToProps = store => ({
  hide: store.hideValuesState.hide,
})

const mapDispatchToProps = dispatch => 
  bindActionCreators({
    setHideValues,
  }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Header)