import React from 'react';

// Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { changeTheme } from '@actions';

// Style
import Style from './style'

// Components
import Sidebar from '../../components/sidebar/'
import Header from '../../components/header/'
import SummaryValues from '../../components/summaryValues/'
import Tab from '../../components/tab/'
import SummaryContent from '../../components/summaryContent/'

function HomePage(props) {
  return (
    <Style>
      <main className="App">
        <section className="login-screen grid-container">
          <Sidebar />
          <Header />
          <SummaryValues />
          <Tab />
          <SummaryContent />
        </section>
      </main>
    </Style>
  )

}

const mapStateToProps = store => ({
  theme: store.themeState.theme,
})

const mapDispatchToProps = dispatch => 
  bindActionCreators({
    changeTheme,
  }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(HomePage)
