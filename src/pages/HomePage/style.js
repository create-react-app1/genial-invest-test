import styled from 'styled-components'

export default styled.div`
  .login-screen.grid-container {
    display: grid;
    grid-template-columns: 18rem 1fr;
    grid-template-rows: 8rem 8rem 6rem 1fr;
    gap: 0;
    grid-template-areas: 
      "sidebar header" 
      "sidebar summary" 
      "sidebar tabs" 
      "sidebar content";
  }
  
  .sidebar { grid-area: sidebar; height: 100%; }
  
  .header { grid-area: header; }
  
  .summary { grid-area: summary; }
  
  .tabs { grid-area: tabs; }
  
  .content { grid-area: content; }

  .login-screen.grid-container * { 
    // border: 1px solid red;
    position: relative;
  }
`