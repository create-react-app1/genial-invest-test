import React from 'react';

// Redux
import { connect } from 'react-redux';

// Components
import Routes from './routes'

// Style Components
import { ThemeProvider } from "styled-components"
import GlobalStyle from "@theme/globalStyle"
import ClassesGlobalStyles from "@theme/classes"
import FontGlobal from '@theme/fonts'

// interface Props {
//   theme: string;
// }

// function App(props: Props) {
//   return (
//     <ThemeProvider theme={{ name: props.theme }}>
//       <Routes />
//       <FontGlobal.Faces />
//       { ClassesGlobalStyles.map((ClassesGlobalStyle: AnyStyledComponent, index: number) => <ClassesGlobalStyle key={index} />) }
//       <GlobalStyle />
//     </ThemeProvider>
//   )
// }

// const mapStateToProps = (store: any) => ({
//   theme: store.themeState.theme,
// })

function App(props) {
  return (
    <ThemeProvider theme={{ name: props.theme }}>
      <Routes />
      <FontGlobal.Faces />
      { ClassesGlobalStyles.map((ClassesGlobalStyle, index) => <ClassesGlobalStyle key={index} />) }
      <GlobalStyle />
    </ThemeProvider>
  )
}

const mapStateToProps = (store) => ({
  theme: store.themeState.theme,
})

export default connect(mapStateToProps, {})(App)
