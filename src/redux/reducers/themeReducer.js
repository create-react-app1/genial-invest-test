import { CHANGE_THEME } from '@actions/actionTypes';

const initialState = {
  theme: 'main'
}

export const themeReducer = (state = initialState, {type, theme}) => {
  switch (type) {
    case CHANGE_THEME:
      return {
        ...state,
        theme: theme
      }
  
    default:
      return state
  }
}

