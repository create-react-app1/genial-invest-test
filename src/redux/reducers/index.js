import { combineReducers } from 'redux';

import { themeReducer } from './themeReducer';
import { hideValuesReducer } from './hideValuesReducer';

export const Reducers = combineReducers({
  themeState: themeReducer,
  hideValuesState: hideValuesReducer,
});