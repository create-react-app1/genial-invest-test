import { SET_HIDE_VALUES } from '@actions/actionTypes';

const initialState = {
    hide: false
};

export const hideValuesReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_HIDE_VALUES:
        return {
            ...state,
            hide: action.hide
        };
        default:
        return state;
    }
};