import { CHANGE_THEME, SET_HIDE_VALUES } from './actionTypes';

export const changeTheme = value => ({
    type: CHANGE_THEME,
    theme: value
})

export const setHideValues = value => ({
    type: SET_HIDE_VALUES,
    hide: value
})